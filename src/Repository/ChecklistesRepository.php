<?php

namespace App\Repository;

use App\Entity\Checklistes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Checklistes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Checklistes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Checklistes[]    findAll()
 * @method Checklistes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChecklistesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Checklistes::class);
    }

    // /**
    //  * @return Checklistes[] Returns an array of Checklistes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Checklistes
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
