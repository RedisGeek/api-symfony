<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\PaysRepository")
 */
class Pays
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Documents", inversedBy="Pays")
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Maladies", mappedBy="Pays")
     */
    private $maladies;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Voyage", inversedBy="Pays")
     */
    private $voyages;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $capital;

    /**
     * @ORM\Column(type="integer")
     */
    private $indicatif_telephonic;

    /**
     * @ORM\Column(type="datetime")
     */
    private $decalage_horaire;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prise_electrique;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $monnaies;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type_permis;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ProfileSante")
     */
    private $profil_santes;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\MonQuotidient")
     */
    private $mon_quotidient;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCapital(): ?string
    {
        return $this->capital;
    }

    public function setCapital(string $capital): self
    {
        $this->capital = $capital;

        return $this;
    }

    public function getIndicatifTelephonic(): ?int
    {
        return $this->indicatif_telephonic;
    }

    public function setIndicatifTelephonic(int $indicatif_telephonic): self
    {
        $this->indicatif_telephonic = $indicatif_telephonic;

        return $this;
    }

    public function getDecalageHoraire(): ?\DateTimeInterface
    {
        return $this->decalage_horaire;
    }

    public function setDecalageHoraire(\DateTimeInterface $decalage_horaire): self
    {
        $this->decalage_horaire = $decalage_horaire;

        return $this;
    }

    public function getPriseElectrique(): ?string
    {
        return $this->prise_electrique;
    }

    public function setPriseElectrique(string $prise_electrique): self
    {
        $this->prise_electrique = $prise_electrique;

        return $this;
    }

    public function getMonnaies()
    {
        return $this->monnaies;
    }

    public function setMonnaies($monnaies): self
    {
        $this->monnaies = $monnaies;

        return $this;
    }

    public function getTypePermis(): ?string
    {
        return $this->type_permis;
    }

    public function setTypePermis(string $type_permis): self
    {
        $this->type_permis = $type_permis;

        return $this;
    }
}
