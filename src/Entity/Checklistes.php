<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ChecklistesRepository")
 */
class Checklistes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_limite_vaccin;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_depart;

    /**
    * @ORM\ManyToMany(targetEntity="App\Entity\Documents", inversedBy="Checklistes")
    */
    private $documents;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDateLimiteVaccin(): ?\DateTimeInterface
    {
        return $this->date_limite_vaccin;
    }

    public function setDateLimiteVaccin(\DateTimeInterface $date_limite_vaccin): self
    {
        $this->date_limite_vaccin = $date_limite_vaccin;

        return $this;
    }

    public function getDateDepart(): ?\DateTimeInterface
    {
        return $this->date_depart;
    }

    public function setDateDepart(\DateTimeInterface $date_depart): self
    {
        $this->date_depart = $date_depart;

        return $this;
    }
}
