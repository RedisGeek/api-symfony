<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\VoyageRepository")
 */
class Voyage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $destination;

    /**
     * @ORM\Column(type="datetime")
     */
    private $historique;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_depart;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_arriver;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mode_paiements;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pays", mappedBy="Voyage")
     */
    private $pays;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDestination(): ?string
    {
        return $this->destination;
    }

    public function setDestination(string $destination): self
    {
        $this->destination = $destination;

        return $this;
    }

    public function getHistorique(): ?\DateTimeInterface
    {
        return $this->historique;
    }

    public function setHistorique(\DateTimeInterface $historique): self
    {
        $this->historique = $historique;

        return $this;
    }

    public function getDateDepart(): ?\DateTimeInterface
    {
        return $this->date_depart;
    }

    public function setDateDepart(\DateTimeInterface $date_depart): self
    {
        $this->date_depart = $date_depart;

        return $this;
    }

    public function getDateArriver(): ?\DateTimeInterface
    {
        return $this->date_arriver;
    }

    public function setDateArriver(\DateTimeInterface $date_arriver): self
    {
        $this->date_arriver = $date_arriver;

        return $this;
    }

    public function getModePaiements(): ?string
    {
        return $this->mode_paiements;
    }

    public function setModePaiements(string $mode_paiements): self
    {
        $this->mode_paiements = $mode_paiements;

        return $this;
    }
}
