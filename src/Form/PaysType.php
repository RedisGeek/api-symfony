<?php

namespace App\Form;

use App\Entity\Pays;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaysType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('capital')
            ->add('indicatif_telephonic')
            ->add('decalage_horaire')
            ->add('prise_electrique')
            ->add('monnaies')
            ->add('type_permis')
            ->add('documents')
            ->add('voyages')
            ->add('profil_santes')
            ->add('mon_quotidient')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pays::class,
        ]);
    }
}
